package fr.supinternet.androidtv.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import fr.supinternet.androidtv.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
    }
}